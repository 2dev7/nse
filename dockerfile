# Use an official Python runtime as a parent image
FROM python:3.8

# Set the working directory in the container
WORKDIR /usr/src/app

# Copy the current directory contents into the container at /usr/src/app
COPY . .

# Install any needed packages specified in requirements.txt
RUN pip install --no-cache-dir -r requirements.txt

# Make port 80 available to the world outside this container
EXPOSE 8080

RUN python manage.py makemigrations
RUN python manage.py migrate

# Run app.py when the container launches
CMD ["python", "manage.py", "runserver", "0.0.0.0:8080"]