Hi Everyone, this application uses a static csv file and populates the data into the Database

prerequisites - install docker and docker daemon should be running

steps to run 
1. make sure you are inside the outer NSE directory
2. run this command on a bash sell "./build.sh"
3. now open the browser and hit this url - "http://localhost:8080/nse-app/option-data/"
