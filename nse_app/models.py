from django.db import models

class OptionData(models.Model):
    oi_calls = models.CharField(max_length=265, default=None, blank=True)
    chng_in_oi_calls = models.CharField(max_length=265, default=None, blank=True)
    volume_calls = models.CharField(max_length=265, default=None, blank=True)
    iv_calls = models.CharField(max_length=265, default=None, blank=True)
    ltp_calls = models.CharField(max_length=265, default=None, blank=True)
    chng_calls = models.CharField(max_length=265, default=None, blank=True)
    bif_qty_calls = models.CharField(max_length=265, default=None, blank=True)
    bid_calls = models.CharField(max_length=265, default=None, blank=True)
    ask_calls = models.CharField(max_length=265, default=None, blank=True)
    ask_qty_calls = models.CharField(max_length=265, default=None, blank=True)
    strike = models.CharField(max_length=265, default=None, blank=True)
    bid_qty_puts = models.CharField(max_length=265, default=None, blank=True)
    bid_puts = models.CharField(max_length=265, default=None, blank=True)
    ask_puts = models.CharField(max_length=265, default=None, blank=True)
    ask_qty_puts = models.CharField(max_length=265, default=None, blank=True)
    chng_puts = models.CharField(max_length=265, default=None, blank=True)
    ltp_puts = models.CharField(max_length=265, default=None, blank=True)
    iv_puts = models.CharField(max_length=265, default=None, blank=True)
    volume_puts = models.CharField(max_length=265, default=None, blank=True)
    chng_in_oi_puts = models.CharField(max_length=265, default=None, blank=True)
    oi_puts = models.CharField(max_length=265, default=None, blank=True)
    

    class Meta:
        db_table = "nse_calls_puts"

    def create_data(data):
        return OptionData.objects.create(**data)
    
    def retreive_data(filter_params):
        return OptionData.objects.filter(**filter_params)
