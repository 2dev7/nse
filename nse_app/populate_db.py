import csv
import os
from .models import OptionData

FILE_PATH = os.path.abspath("nse_app/options_data.csv")

class DatabaseHandler:

    @staticmethod
    def csv_to_dict_list(file_path):
        data = []
        with open(file_path, newline='') as csvfile:
            reader = csv.DictReader(csvfile, delimiter=';')
            for row in reader:
                # Replace '-' with an empty string for each value in the row
                cleaned_row = {key: value if value != '-' else '' for key, value in row.items()}
                data.append(cleaned_row)
        return data


    @staticmethod
    def populate_data_in_db():
        objects = DatabaseHandler.csv_to_dict_list(FILE_PATH)
        [item.pop('') for item in objects]
        objects = [OptionData(**item) for item in objects]
        OptionData.objects.bulk_create(objects)

# DatabaseHandler.populate_data_in_db()
