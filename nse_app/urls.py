from django.urls import path
from nse_app.views import option_data_list

urlpatterns = [
    path('option-data/', option_data_list, name='option_data_list'),
    # Add more URL patterns as needed
]