from django.shortcuts import render
from ..models import OptionData
from ..populate_db import DatabaseHandler

def option_data_list(request):

    DatabaseHandler.populate_data_in_db()
        if not OptionData.objects.exists():
    # Fetch all OptionData objects from the database
    option_data_list = OptionData.objects.all()
    # template = loader.get_template('myfirst.html')
    return render(request, 'nse_app/option_data_list.html', {'option_data_list': option_data_list})
